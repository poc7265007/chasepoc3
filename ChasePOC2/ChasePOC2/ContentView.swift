//
//  ContentView.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewmodel = SchoolsViewModel()

    @State var query: String = ""
    var body: some View {
        VStack {
            if viewmodel.isLoading {
                ProgressView()
            } else {
                if viewmodel.schools.isEmpty == false {
                    VStack(alignment: .leading) {
                        List {
                            ForEach(viewmodel.schools, id: \.self) { school in
                                VStack(alignment: .leading, spacing: 12) {
                                    Text(school.schoolName ?? "")
                                        .font(.title)
                                        .fontWeight(.bold)
                                    Text(school.overviewParagraph ?? "")
                                        .font(.body)
                                        .fontWeight(.medium)
                                }.onTapGesture {
                                    viewmodel.selectedSchool(dbn: school.dbn ?? "")
                                }
                            }
                        }
                    }
                    .sheet(item: $viewmodel.selectedSchoolSatScore) { selectedPost in
                        Text(selectedPost.satWritingAvgScore ?? "")
                    }
                    .alert(isPresented: $viewmodel.noSatScoreAvailable) {
                                    Alert(title: Text("Error"),
                                        message: Text("SAT Score not available for selected school"),
                                        dismissButton: .default(Text("OK")))
                                }
                }
            }
        }
        .onAppear {
            Task {
                viewmodel.getSchools()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
