//
//  SchoolAPIService.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import Foundation

class SchoolAPIService {
    static let shared = SchoolAPIService()
    
    func fetchSchools(from: URL, result: @escaping(Result<[School], APIRequestError>) -> Void) {
        let req = API(url: from)
        req.perform(with: result)
    }
    
    func fetchSATScores(from: URL, result: @escaping(Result<[SATDetails], APIRequestError>) -> Void) {
        let req = API(url: from)
        req.perform(with: result)
    }
}
